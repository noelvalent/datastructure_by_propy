//
// Created by Noel Valentine on 2019-03-05.
//

#include <stdio.h>
#include "ArrayBaseStack.h"

int main(int argc, const char *args[])
{
    Stack stack;
    StackInit(&stack);
    int i;
    for(i=1;i<6;i++)
        SPush(&stack, i);

    while(!SIsEmpty(&stack))
        printf("%d \n", SPop(&stack));

    return 0;
}

