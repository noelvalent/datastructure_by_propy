//
// Created by Noel Valentine on 2019-03-05.
//

#ifndef DATASTRUCTURE_BY_PROPY_ARRAYBASESTACK_H
#define DATASTRUCTURE_BY_PROPY_ARRAYBASESTACK_H

#define TRUE 1
#define FALSE 0

#define STACK_LEN 100

typedef int Data;
typedef struct _arrStack{
    Data stackArr[STACK_LEN];
    int topIndex;
} ArrayStack;

typedef ArrayStack Stack;

void StackInit(Stack *pstack);
int SIsEmpty(Stack *pstack);

void SPush(Stack *pstack, Data data);
Data SPop(Stack *pstack);
Data SPeek(Stack *pstack);


#endif //DATASTRUCTURE_BY_PROPY_ARRAYBASESTACK_H
