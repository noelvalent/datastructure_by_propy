//
// Created by Noel Valentine on 2019-03-08.
//

#include "SimpleHeap.h"
#include <stdio.h>
int Compare(char ch1, char ch2){
    //return ch2-ch1;
    return ch1 - ch2;
}


int main(int argc, const char * args[]){
    Heap heap;
    HeapInit(&heap, Compare);
    HInsert(&heap, 'A');
    HInsert(&heap, 'B');
    HInsert(&heap, 'C');

    printf("%c \n", HDelete(&heap));

    HInsert(&heap, 'A');
    HInsert(&heap, 'B');
    HInsert(&heap, 'C');
    printf("%c \n", HDelete(&heap));

    printf("\n\n");

    while(!HIsEmpty(&heap)){
        printf("%c \n", HDelete(&heap));
    }
    return 0;

}