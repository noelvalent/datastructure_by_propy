//
//  ArrayList.c
//  C_Data_Struct
//
//  Created by Noel Valentine on 05/01/2019.
//  Copyright © 2019 Noel Valentine. All rights reserved.
//

#include "ArrayList.h"
#include <stdio.h>
void ListInit(List *plist)
{
    (plist->numOfData) = 0; //We don't have any data

    (plist->curPosition) = -1; //Null Pointer


}
void LInsert(List *plist, LData data)
{
    if(plist->numOfData >= LIST_LEN)
    {
        printf("We Can Not More Data!!!");
        return;
    }
    plist->arr[plist->numOfData] = data;
    (plist->numOfData)++;
}

int LFirst(List *plist, LData *pdata)
{
    if(plist->numOfData == 0)
    {
        return FALSE;
    }

    (plist->curPosition) = 0;
    *pdata = plist->arr[0];
    return TRUE;
}

int LNext(List *plist, LData *pdata){
    if(plist->curPosition >= (plist->numOfData)-1)
        return FALSE;

    (plist->curPosition)++;
    *pdata = plist->arr[plist->curPosition];
    return TRUE;
}

LData LRemove(List *plist){
    int i;
    int rpos = plist->curPosition;
    int num = plist->numOfData;
    LData rdata = plist->arr[rpos];

    for(i=rpos;i<num-1;i++)
        plist->arr[i] = plist->arr[i+1];

    (plist->numOfData)--;
    (plist->curPosition)--;
    return rdata;
}

int LCount(List *plist)
{
    return plist->numOfData;
}
