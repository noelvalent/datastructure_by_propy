//
//  main.c
//  DataStructure_by_propY
//
//  Created by Noel Valentine on 02/03/2019.
//  Copyright © 2019 Noel Valentine. All rights reserved.
//
#include "../DLinkedList/DLinkedList.h"
#include <stdio.h>

int main(int argc, const char * argv[]) {
    // insert code here...
    printf("Hello, World!\n");

    List list;
    int data;
    ListInit(&list);


    LInsert(&list, 11);
    LInsert(&list, 11);
    LInsert(&list, 22);
    LInsert(&list, 22);
    LInsert(&list, 33);


    printf("현재 데이터의 수: %d \n", LCount(&list));

    if(LFirst(&list, &data))
    {
        printf("%d ", data);
        while (LNext(&list, &data)) {
            printf("%d ", data);
        }
    }
    printf("\n\n");


    if(LFirst(&list, &data))
    {
        if(data == 22)
        {
            LRemove(&list);
        }

        while (LNext(&list, &data)) {
            if(data == 22)
                LRemove(&list);
        }
    }
    printf("남은 데이터의 수: %d \n", LCount(&list));

    if(LFirst(&list, &data))
    {
        printf("%d ",data);

        while(LNext(&list, &data))
            printf("%d ", data);
    }
    printf("\n\n");




    return 0;
}
