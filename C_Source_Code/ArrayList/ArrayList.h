//
//  ArrayList.h
//  C_Data_Struct
//
//  Created by Noel Valentine on 05/01/2019.
//  Copyright © 2019 Noel Valentine. All rights reserved.
//

#ifndef ArrayList_h
#define ArrayList_h
#define TRUE 1
#define FALSE 0

#define LIST_LEN 100
typedef int LData;

typedef struct __ArrayList
{
    LData arr[LIST_LEN];
    int numOfData;
    int curPosition;
} ArrayList;

typedef ArrayList List;

void ListInit(List * plist);
void LInsert(List * plist, LData data);

int LFirst(List *plist, LData *pdata);
int LNext(List *plist, LData *pdata);

LData LRemove(List *plist);
int LCount(List *plist);





#endif /* ArrayList_h */
