//
// Created by Noel Valentine on 2019-03-07.
//

#ifndef DATASTRUCTURE_BY_PROPY_BINARYTREE_H
#define DATASTRUCTURE_BY_PROPY_BINARYTREE_H
typedef  int BTData;

typedef struct _bt{
    BTData data;
    struct _bt *left;
    struct _bt *right;
} BTreeNode;

typedef void VisitFuncPtr(BTData data);

BTreeNode* MakeBTreeNode(void);
BTData GetData(BTreeNode *bt);
void SetData(BTreeNode *bt, BTData data);

BTreeNode* GetLeftSubTree(BTreeNode *bt);
BTreeNode* GetRightSubTree(BTreeNode *bt);

void MakeLeftSubTree(BTreeNode *main, BTreeNode *sub);
void MakeRightSubTree(BTreeNode *main, BTreeNode *sub);

void InorderTraverse(BTreeNode *bt, VisitFuncPtr action);
void PreorderTraverse(BTreeNode *bt, VisitFuncPtr action);
void PostorderTraverse(BTreeNode *bt, VisitFuncPtr action);
void DeleteTree(BTreeNode* bt);

#endif //DATASTRUCTURE_BY_PROPY_BINARYTREE_H
