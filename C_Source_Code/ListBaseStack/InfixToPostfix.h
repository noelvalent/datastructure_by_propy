//
// Created by Noel Valentine on 2019-03-05.
//

#ifndef DATASTRUCTURE_BY_PROPY_INFIXTOPOSTFIX_H
#define DATASTRUCTURE_BY_PROPY_INFIXTOPOSTFIX_H
#include <string.h>
#include <ctype.h>
#include <stdlib.h>

int GetOpPrec(char op);
int WhoPrecOp(char op1, char op2);
void ConvToRPNExp(char exp[]);
int EvalRPNExp(char exp[]);
#endif //DATASTRUCTURE_BY_PROPY_INFIXTOPOSTFIX_H
