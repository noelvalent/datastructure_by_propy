//
// Created by Noel Valentine on 2019-03-05.
//

#ifndef DATASTRUCTURE_BY_PROPY_LISTBASESTACK_H
#define DATASTRUCTURE_BY_PROPY_LISTBASESTACK_H

#include "../BinaryTree/BinaryTree.h"
#define TRUE 1
#define FALSE 0

//typedef int Data;
typedef BTreeNode* Data;
typedef struct _node{
    Data data;
    struct _node *next;
} Node;

typedef struct _lstStack{
    Node * head;
} ListStack;

typedef ListStack Stack;

void StackInit(Stack *pstack);
int SIsEmpty(Stack *pstack);

void SPush(Stack *pstack, Data data);
Data SPop(Stack *pstack);
Data SPeek(Stack *pstack);

#endif //DATASTRUCTURE_BY_PROPY_LISTBASESTACK_H
