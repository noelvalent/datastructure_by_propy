//
// Created by Noel Valentine on 2019-03-07.
//

#include <stdio.h>
#include "ExpressionTree.h"

int main(int argc, const char* args[]){

    char exp[] = "327*+";
    BTreeNode *eTree = MakeExpTree(exp);


    /*
    printf("전위 표기법의 수식\n");
    ShowPrefixTypeExp(eTree);
    printf("\n");

    printf("후위 표기법의 수식\n");
    ShowPostfixTypeExp(eTree);
    printf("\n");
    */
    printf("연산 결과: %d\n", EvaluateExpTree(eTree));

    printf("중위 표기법의 수식\n");
    ShowInfixTypeExp(eTree);
    printf("\n");
    return 0;


}
