//
// Created by Noel Valentine on 2019-03-07.
//

#ifndef DATASTRUCTURE_BY_PROPY_EXPRESSIONTREE_H
#define DATASTRUCTURE_BY_PROPY_EXPRESSIONTREE_H

#include "../BinaryTree/BinaryTree.h"

BTreeNode *MakeExpTree(char exp[]);
int EvaluateExpTree(BTreeNode *bt);

void ShowPrefixTypeExp(BTreeNode *bt);
void ShowInfixTypeExp(BTreeNode *bt);
void ShowPostfixTypeExp(BTreeNode *bt);


#endif //DATASTRUCTURE_BY_PROPY_EXPRESSIONTREE_H
