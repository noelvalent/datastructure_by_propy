//
// Created by Noel Valentine on 2019-03-06.
//

#include "ListBaseQueue.h"
#include <stdio.h>

int main(int argc, const char* args[]){
    int i;
    Queue q;
    QueueInit(&q);

    for(i=1;i<6;i++) Enqueue(&q, i);

    while(!QIsEmpty(&q)) printf("%d ", Dequeue(&q));

    return 0;
}