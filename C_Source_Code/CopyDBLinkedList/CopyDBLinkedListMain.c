//
// Created by Noel Valentine on 2019-03-05.
//

#include <stdio.h>
#include "CopyDBLinkedList.h"

int main(int argc, const char *argv[] ){
    List list;
    int data, i;

    ListInit(&list);
    for(i=1;i<11;i++)
        LInsert(&list, i);

    printf("Current Items Count %d\n\n", LCount(&list));

    if(LFirst(&list, &data)){
        printf("%d ", data);
        while(LNext(&list, &data)){
            printf("%d ", data);
        }
        printf("\n\n");
    }

    if(LFirst(&list, &data)){
        if(data%3==0)
            LRemove(&list);
        while(LNext(&list, &data)){
            if(data%3==0)
                LRemove(&list);
        }
    }

    if(LFirst(&list, &data)){
        printf("%d ", data);
        while(LNext(&list, &data)){
            printf("%d ", data);
        }
        printf("\n\n");
    }

    printf("Current Items Count %d\n", LCount(&list));
}