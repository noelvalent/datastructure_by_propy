//
// Created by Noel Valentine on 2019-03-04.
//

#ifndef DATASTRUCTURE_BY_PROPY_COPYDBLINKEDLIST_H
#define DATASTRUCTURE_BY_PROPY_COPYDBLINKEDLIST_H

#define TRUE 1;
#define FALSE 0;
typedef int Data;

typedef struct _node {
    Data data;
    struct _node *next;
    struct _node *prev;
} Node;

typedef struct _dll{
    Node *head;
    Node *tail;
    Node *cur;
    int numOfData;
} CopyedDBLList;

typedef CopyedDBLList List;

void ListInit(List *plist);
void LInsert(List *plist, Data data);

int LFirst(List *plist, Data *data);
int LNext(List *plist, Data *data);

Data LRemove(List *plist);
int LCount(List *plist);

#endif //DATASTRUCTURE_BY_PROPY_COPYDBLINKEDLIST_H
