//
// Created by Noel Valentine on 2019-03-04.
//

#ifndef DATASTRUCTURE_BY_PROPY_CLINKEDLIST_H
#define DATASTRUCTURE_BY_PROPY_CLINKEDLIST_H
#define TRUE 1
#define FALSE 0

typedef int Data;

typedef struct _node{
    Data data;
    struct _node *next;
} Node;

typedef struct _CLL
{
    Node *tail;
    Node *cur;
    Node *before;
    int numOfData;
} CList;

typedef CList List;

void ListInit(List * plist);
void LInsert(List * plist, Data data);
void LInsertFront(List *plist, Data data);

int LFirst(List *plist, Data *data);
int LNext(List *plist, Data *data);
Data LRemove(List *plist);
int LCount(List *plist);
int LLast(List *plist, Data *data);

#endif //DATASTRUCTURE_BY_PROPY_CLINKEDLIST_H
