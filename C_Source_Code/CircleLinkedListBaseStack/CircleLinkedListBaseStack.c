//
// Created by Noel Valentine on 2019-03-05.
//

#include "CircleLinkedListBaseStack.h"
#include <stdlib.h>

void StackInit(Stack *pstack){
    pstack->head = (List*)malloc(sizeof(List));
    ListInit(pstack->head);
}
int SIsEmpty(Stack *pstack){
    if(LCount(pstack->head) == 0){
        return TRUE;
    } else {
        return FALSE;
    }
}

void SPush(Stack *pstack, Data data){
    LInsert(pstack->head, data);

}
Data SPop(Stack *pstack){
    int rdata;
    LLast(pstack->head, &rdata);
    LRemove(pstack->head);
    return rdata;

}
Data SPeek(Stack *pstack){
    int rdata;
    LLast(pstack->head, &rdata);
    return rdata;
}