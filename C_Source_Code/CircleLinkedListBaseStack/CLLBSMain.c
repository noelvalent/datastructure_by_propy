//
// Created by Noel Valentine on 2019-03-05.
//

#include <stdio.h>
#include "CircleLinkedListBaseStack.h"

int main(int argc, const char* args[]){
    Stack stack;
    int i;
    printf("Hello World\n");
    StackInit(&stack);
    for(i=1;i<6;i++)
        SPush(&stack, i);

    printf("%s \n", SIsEmpty(&stack)?"Empty":"Not Empty");
    printf("%d \n", SPeek(&stack));
    while(!SIsEmpty(&stack))
        printf("%d \n", SPop(&stack));

    printf("%s \n", SIsEmpty(&stack)?"Empty":"Not Empty");

    return 0;

}