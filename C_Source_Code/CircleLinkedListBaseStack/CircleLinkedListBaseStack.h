//
// Created by Noel Valentine on 2019-03-05.
//

#ifndef DATASTRUCTURE_BY_PROPY_CIRCLELINKEDLISTBASESTACK_H
#define DATASTRUCTURE_BY_PROPY_CIRCLELINKEDLISTBASESTACK_H

#include "../CLinkedList/CLinkedList.h"

typedef struct _cllbs{
    List *head;
} CircleLinkedListBaseStack;

typedef CircleLinkedListBaseStack Stack;

void StackInit(Stack *pstack);
int SIsEmpty(Stack *pstack);

void SPush(Stack *pstack, Data data);
Data SPop(Stack *pstack);
Data SPeek(Stack *pstack);


#endif //DATASTRUCTURE_BY_PROPY_CIRCLELINKEDLISTBASESTACK_H
