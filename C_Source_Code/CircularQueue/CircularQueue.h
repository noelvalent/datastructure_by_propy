//
// Created by Noel Valentine on 2019-03-06.
//

#ifndef DATASTRUCTURE_BY_PROPY_CIRCULARQUEUE_H
#define DATASTRUCTURE_BY_PROPY_CIRCULARQUEUE_H
#define TRUE 1
#define FALSE 0
#define QUEUE_LEN 100

typedef int Data;
typedef struct _cQ{
    int front;
    int rear;
    Data queArr[QUEUE_LEN];
} CQueue;

typedef CQueue Queue;

void QueueInit(Queue *pq);
int QIsEmpty(Queue *pq);

void Enqueue(Queue *pq, Data data);
Data Dequeue(Queue *pq);
Data QPeek(Queue *pq);



#endif //DATASTRUCTURE_BY_PROPY_CIRCULARQUEUE_H
