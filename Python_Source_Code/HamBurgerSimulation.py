import ListBaseQueue
import random as rd

CUS_COME_TERM = 15

CHE_BUR = 0
BUL_BUR = 1
DUB_BUR = 2

CHE_TERM = 12
BUL_TERM = 15
DUB_TERM = 24

LIMIT = 100000



for i in range(1, LIMIT):
    que = ListBaseQueue.ListQueue()
    makeProc = 0
    cheOrder = 0
    bulOrder = 0
    dubOrder = 0
    
    for sec in range(0, 3601):
        if sec % CUS_COME_TERM == 0:
            sel = rd.randrange(0, 3)
            if sel == CHE_BUR:
                que.enQueue(CHE_TERM)
                cheOrder += 1
            elif sel == BUL_BUR:
                que.enQueue(BUL_TERM)
                bulOrder += 1
            elif sel == DUB_BUR:
                que.enQueue(DUB_TERM)
                dubOrder += 1
        if makeProc <= 0 and not que.isEmpty():
            makeProc = que.deQueue()

        makeProc -= 1

    print("------------------------------------\n")
    print("\tSimulation Report!\n")
    print(" - Cheese Burger: {} \n".format(cheOrder))
    print(" - Bulgogi Burger: {} \n".format(bulOrder))
    print(" - Double Burger: {} \n".format(dubOrder))
    print(" - Count: {}\n".format(i))
    print("------------------------------------\n")
    del(que)
