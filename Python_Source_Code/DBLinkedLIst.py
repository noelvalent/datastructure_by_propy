class Node(object):
    def __init__(self, data=None, nextNode = None, prevNode = None):
        self.data = data
        self.nextNode = nextNode
        self.prevNode = prevNode

class DBLinkedList(object):
    def __init__(self):
        self.head = None
        self.cur = None
        self.numOfData = 0

    def LInsert(self, data):
        newNode = Node(data)
        newNode.nextNode = self.head

        if self.head != None:
            self.head.prevNode = newNode

        newNode.prevNode = None
        self.head = newNode
        self.numOfData += 1

    def LFirst(self, ldata):
        if self.head == None:
            return False

        self.cur = self.head
        ldata[0] = self.cur.data

        return True

    def LNext(self, ldata):
        if self.cur.nextNode == None:
            return False

        self.cur = self.cur.nextNode
        ldata[0] = self.cur.data
        return True

    def LPrevious(self, ldata):
        if self.cur.prevNode == None:
            return False

        self.cur = self.cur.prevNode
        ldata[0] = self.cur.data
        return True

    def LCount(self):
        return self.numOfData


if __name__ == '__main__':
    dbll = DBLinkedList()
    lstData = [0,]

    for i in range(1, 10):
        dbll.LInsert(i)

    if dbll.LFirst(lstData):
        print("{}".format(lstData[0]))
        while dbll.LNext(lstData):
            print("{}".format(lstData[0]))

        while dbll.LPrevious(lstData):
            print("{}".format(lstData[0]))

        print('\n\n')