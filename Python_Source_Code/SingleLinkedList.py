

class Node(object):
    def __init__(self, data=None, nextNode = None):
        self.data = data
        self.nextNode = nextNode

class LinkedList(object):
    def __init__(self):
        self.head = Node()
        self.cur = None
        self.before = None
        self.numOfData = 0
        self.comp = None

    def FInsert(self, data):
        newNode = Node(data)
        newNode.nextNode = self.head.nextNode
        self.head.nextNode = newNode
        self.numOfData += 1

    def SInsert(self, data):
        newNode = Node(data)
        pred = self.head

        while (pred.nextNode != None) and (self.comp(data, pred.nextNode.data) != 0):
            pred = pred.nextNode

        newNode.nextNode = pred.nextNode
        pred.nextNode = newNode
        self.numOfData += 1

    def LInsert(self, data):
        if self.comp == None:
            self.FInsert(data)
        else:
            self.SInsert(data)

    def LFirst(self,ldata):
        if self.head.nextNode == None:
            return False

        self.before = self.head
        self.cur = self.head.nextNode
        ldata[0] = self.cur.data
        return True

    def LNext(self, ldata):
        if self.cur.nextNode == None:
            return False

        self.before = self.cur
        self.cur = self.cur.nextNode
        ldata[0] = self.cur.data
        return True

    def LRemove(self):
        rpos = self.cur
        rdata = rpos.data

        self.before.nextNode = self.cur.nextNode
        self.cur = self.before

        del(rpos)
        self.numOfData -= 1

    def LCount(self):
        return self.numOfData

    def SetSortRule(self, comp):
        self.comp = comp

def compareNums(d1, d2):
    if d1 < d2:
        return 0
    else:
        return 1

if __name__ == '__main__':
    print('this is main routine')
    ldata = [0]
    sll = LinkedList()
    sll.LInsert(11)
    sll.LInsert(11)
    sll.LInsert(22)
    sll.LInsert(22)
    sll.LInsert(33)
    print("Current Data's Count {}".format(sll.LCount()))
    if sll.LFirst(ldata):
        print("{} ".format(ldata[0]))
        while sll.LNext(ldata):
            print("{} ".format(ldata[0]))

    print('\n\n')
    if sll.LFirst(ldata):
        if ldata[0] == 22:
            sll.LRemove()
        while sll.LNext(ldata):
            if ldata[0] == 22:
                sll.LRemove()

    print("Current Data's Count {}".format(sll.LCount()))
    if sll.LFirst(ldata):
        print("{} ".format(ldata[0]))
        while sll.LNext(ldata):
            print("{} ".format(ldata[0]))

    print('\n\n')

    sll.SetSortRule(compareNums)
    for i in range(1, 11):
        sll.LInsert(i)

    print("Current Data's Count {}".format(sll.LCount()))
    if sll.LFirst(ldata):
        print("{} ".format(ldata[0]))
        while sll.LNext(ldata):
            print("{} ".format(ldata[0]))


