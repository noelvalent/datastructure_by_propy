class Node(object):
    def __init__(self, data=None, nextNode = None):
        self.data = data
        self.nextNode = nextNode

class CircleLinkedList(object):
    def __init__(self):
        self.tail = None
        self.cur = None
        self.before = None
        self.numOfData = 0

    def LInsert(self, data):
        newNode = Node(data)
        if self.tail == None:
            self.tail = newNode
            newNode.nextNode = newNode
        else:
            newNode.nextNode = self.tail.nextNode
            self.tail.nextNode = newNode
            self.tail = newNode
        self.numOfData += 1

    def LInsertFront(self, data):
        newNode = Node(data)
        if self.tail == None:
            self.tail = newNode
            newNode.nextNode = newNode
        else:
            newNode.nextNode = self.tail.nextNode
            self.tail.nextNode = newNode
        self.numOfData += 1

    def LFirst(self, ldata):
        if self.tail == None:
            return False

        self.before = self.tail
        self.cur = self.tail.nextNode

        ldata[0] = self.cur.data
        return True

    def LNext(self, ldata):
        if self.tail == None:
            return False

        self.before = self.cur
        self.cur = self.cur.nextNode

        ldata[0] = self.cur.data
        return True

    def LRemove(self):
        rpos = self.cur
        rdata = rpos.data

        if rpos == self.tail:
            if self.tail == self.tail.nextNode:
                self.tail = None
            else:
                self.tail = self.before
        self.before.nextNode = self.cur.nextNode
        self.cur = self.before
        del(rpos)
        self.numOfData -= 1
        return rdata

    def LCount(self):
        return self.numOfData


if __name__ == '__main__':
    cll = CircleLinkedList()
    lstData = [0,]
    nodeNum = 0

    cll.LInsert(3)
    cll.LInsert(4)
    cll.LInsert(5)
    cll.LInsertFront(2)
    cll.LInsertFront(1)

    if cll.LFirst(lstData):
        print(lstData[0])
        for i in range(0, cll.LCount()*3-1):
            if cll.LNext(lstData):
                print(lstData[0])
    print('')
    nodeNum = cll.LCount()
    if cll.LFirst(lstData):
        if lstData[0] % 2 == 0:
            cll.LRemove()

        for i in range(0, nodeNum-1):
            cll.LNext(lstData)
            if lstData[0] % 2 == 0:
                cll.LRemove()

    if cll.LFirst(lstData):
        print(lstData[0])
        for i in range(0, cll.LCount()-1):
            if cll.LNext(lstData):
                print(lstData[0])
