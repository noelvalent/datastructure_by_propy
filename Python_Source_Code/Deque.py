class Node(object):
    def __init__(self, data=None, nextNode = None, prevNode = None):
        self.data = data
        self.nextNode = nextNode
        self.prevNode = prevNode


class DLDeque(object):
    def __init__(self):
        self.head = None
        self.tail = None

    def isEmpty(self):
        if self.head == None:
            return True
        else:
            return False

    def addFirst(self, data):
        newNode = Node(data)
        newNode.nextNode = self.head
        if self.isEmpty():
            self.tail = newNode
        else:
            self.head.prevNode = newNode
        newNode.prevNode = None
        self.head = newNode

    def addLast(self, data):
        newNode = Node(data)
        newNode.prevNode = self.tail
        if self.isEmpty():
            self.head = newNode
        else:
            self.tail.nextNode = newNode
        newNode.nextNode = None
        self.tail = newNode

    def removeFirst(self):
        if self.isEmpty():
            print("Mem Err")
            quit(-1)

        rnode = self.head
        rdata = self.head.data
        self.head = self.head.nextNode
        del(rnode)
        if self.head == None:
            self.tail = None
        else:
            self.head.prevNode = None
        return rdata

    def removeLast(self):
        if self.isEmpty():
            print("Mem Err")
            quit(-1)

        rnode = self.tail
        rdata = self.tail.data
        self.tail = self.tail.prevNode
        del(rnode)
        if self.tail == None:
            self.head = None
        else:
            self.tail.nextNode = None
        return rdata

    def getFirst(self):
        if self.isEmpty():
            print("Mem Err")
            quit(-1)

        return self.head.data

    def getLast(self):
        if self.isEmpty():
            print("Mem Err")
            quit(-1)

        return self.tail.data


if __name__ == '__main__':
    dq = DLDeque()

    dq.addFirst(3)
    dq.addFirst(2)
    dq.addFirst(1)
    dq.addLast(4)
    dq.addLast(5)
    dq.addLast(6)

    while not dq.isEmpty():
        print('{}'.format(dq.removeFirst()))

    print('\n')

    dq.addFirst(3)
    dq.addFirst(2)
    dq.addFirst(1)
    dq.addLast(4)
    dq.addLast(5)
    dq.addLast(6)

    while not dq.isEmpty():
        print('{}'.format(dq.removeLast()))

    print('\n')

