class Node(object):
    def __init__(self, data=None, nextNode = None):
        self.data = data
        self.nextNode = nextNode

class ListQueue(object):
    def __init__(self):
        self.front = None
        self.rear = None

    def isEmpty(self):
        if self.front == None:
            return True
        else:
            return False

    def enQueue(self, data):
        newNode = Node(data)
        newNode.nextNode = None

        if self.isEmpty():
            self.front = newNode
            self.rear = newNode
        else:
            self.rear.nextNode = newNode
            self.rear = newNode

    def deQueue(self):
        if self.isEmpty():
            print("Mem Err")
            quit(-1)

        delNode = self.front
        rdata = delNode.data
        self.front = self.front.nextNode
        del(delNode)
        return rdata

    def peek(self):
        if self.isEmpty():
            print("Mem Err");
            quit(-1)
        return self.front.data


if __name__ == '__main__':
    que = ListQueue()

    for i in range(1, 6):
        que.enQueue(i)

    while not que.isEmpty():
        print("{} ".format(que.deQueue()))