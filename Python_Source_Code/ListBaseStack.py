class Node(object):
    def __init__(self, data=None, nextNode = None):
        self.data = data
        self.nextNode = nextNode


class ListStack(object):
    def __init__(self):
        self.head = None

    def IsEmpty(self):
        if self.head == None:
            return True
        else:
            return False

    def Push(self, data):
        newNode = Node(data)
        newNode.nextNode = self.head
        self.head = newNode

    def Pop(self):
        if self.IsEmpty():
            print("Mem Err")
            quit(-1)
        rdata = self.head.data
        rnode = self.head

        self.head = self.head.nextNode
        del(rnode)
        return rdata

    def Peek(self):
        if self.IsEmpty():
            print("Mem Err")
            quit(-1)
        return self.head.data


if __name__ == '__main__':
    stk = ListStack()
    for i in range(1, 6):
        stk.Push(i)

    while not stk.IsEmpty():
        print(stk.Pop())
